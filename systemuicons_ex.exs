Mix.install([
  {:jason, "~> 1.3"}
])

repo_url = "https://github.com/CoreyGinnivan/system-uicons.git"
System.shell("rm -rf ./*", cd: "./priv")
System.cmd("git", ["clone", "--depth=1", repo_url], cd: "./priv")

path = "./priv/system-uicons/src/js/data.js"
source_data = File.read!(path)

<<"var sourceData = "::utf8 , rest::binary>> = source_data
# remove trailing semicolon
sslice = String.slice(rest, 0..-3//1)
# quote object keys
quote_keys = Regex.replace(~r/([\w_]+):/, sslice, "\"\\1\":")
# remove trailing commas
rm_trailing_commas = Regex.replace(~r/,\s+(}|])/, quote_keys, "\\1")
icon_data = Jason.decode!(rm_trailing_commas)

icon_map = Enum.map(icon_data, fn %{"icon_path" => path} = icon ->
  svg = File.read!("./priv/system-uicons/src/images/icons/#{path}.svg")
  Map.put_new(icon, "icon_svg", svg)
  |> Map.new(fn {k, v} -> {String.to_atom(k), v} end)
end)

defmodule Templates do
  def svg(name, contents, path, keywords) do
    """
    @doc "Name: #{name} Keywords: #{keywords}"
    def icon_#{path}(assigns) do
    \"\"\"
    <i class="svg">
    """
    <> contents <>
    """

    </i>
    \"\"\"
    end

    """
  end
end

funs = for %{icon_name: name, icon_path: path, icon_svg: svg, icon_keywords: tags} <- icon_map do
  Templates.svg(name, svg, path, tags)
end
|> Enum.join()

content = "defmodule do\n" <> funs <> "\nend\n"
File.write!("./icons.ex", content)
